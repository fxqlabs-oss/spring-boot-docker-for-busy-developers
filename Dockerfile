FROM openjdk:8-jre

EXPOSE 8080

COPY /build/libs/docker-app-*.jar /opt/fxqlabs-oss/docker-app/docker-app.jar

HEALTHCHECK --interval=30s --timeout=3s CMD curl -f http://localhost:8080/actuator/health || exit 1

ENTRYPOINT [\
    "java",\
    "-jar",\
    "/opt/fxqlabs-oss/docker-app/docker-app.jar"\
    ]